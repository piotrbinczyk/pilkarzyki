<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayerHasTeamRepository")
 */
class PlayerHasTeam
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="player_has_team_id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $playerHasTeamId;

    /**
     * @var int
     *
     * @ORM\Column(name="fk_player_id", type="integer", nullable=false)
     *
     */
    private $fkPlayerId;

    /**
     * @var int
     *
     * @ORM\Column(name="fk_team_id", type="integer", nullable=false)
     *
     */
    private $fkTeamId;

    public function setFkPlayerId(?int $fkPlayerId): self
    {
        $this->fkPlayerId = $fkPlayerId;

        return $this;
    }

    public function setFkTeamId(?int $fkTeamId): self
    {
        $this->fkTeamId = $fkTeamId;

        return $this;
    }
}
