<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeamHasMatchRepository")
 */
class TeamHasMatch
{

    /**
     * @var int
     *
     * @ORM\Column(name="team_has_match_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $teamHasMatchId;

    /**
     * @var int
     *
     * @ORM\Column(name="fk_team_id", type="integer", nullable=false)
     *
     */
    private $fkTeamId;

    /**
     * @var int
     *
     * @ORM\Column(name="fk_match_id", type="integer", nullable=false)
     *
     */
    private $fkMatchId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="points", type="smallint", nullable=false, options={"default"="0","unsigned"=true})
     */
    private $points;

    /**
     * @var bool
     *
     * @ORM\Column(name="result", type="boolean", nullable=false)
     */
    private $result;

    public function setFkTeamId(int $fkTeamId): self
    {
        $this->fkTeamId = $fkTeamId;

        return $this;
    }

    public function setFkMatchId(int $fkMatchId): self
    {
        $this->fkMatchId = $fkMatchId;

        return $this;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function setResult(bool $result): self
    {
        $this->result = $result;

        return $this;
    }
}
