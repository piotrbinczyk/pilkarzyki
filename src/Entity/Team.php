<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 */
class Team
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="team_id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $teamId;

    public function getTeamId(): ?int
    {
        return $this->teamId;
    }
}
