<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Season
 *
 * @ORM\Entity(repositoryClass="App\Repository\SeasonRepository")
 */
class Season
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="season_id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $seasonId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $endTime;

    public function getSeasonId(): ?int
    {
        return $this->seasonId;
    }

    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }
}
