<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Player
 *
 * @ORM\Entity(repositoryClass="App\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="player_id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $playerId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 3,
     *      max = 30,
     *      minMessage = "Nazwa musi zawierać co najmniej {{ limit }} znaki",
     *      maxMessage = "Nazwa może zawierać maksymalnie {{ limit }} znaków"
     * )
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="points", type="smallint", nullable=true, options={"default"="0","unsigned"=true})
     */
    private $points;

    /**
     * @var int
     *
     * @ORM\Column(name="fk_season_id", type="integer", nullable=false)
     *
     */
    private $fkSeasonId;

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setPoints(?int $points): self
    {
        $this->points = $points;

        return $this;
    }

    public function setFkSeasonId(int $fkSeasonId): self
    {
        $this->fkSeasonId = $fkSeasonId;

        return $this;
    }
}
