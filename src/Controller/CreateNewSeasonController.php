<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\NewSeason\NewSeasonService;

/**
 * @Route("/create-new-season")
 */
class CreateNewSeasonController extends AbstractController
{
    private $newSeasonService;

    public function __construct(NewSeasonService $newSeasonService)
    {
        $this->newSeasonService = $newSeasonService;
    }

    /**
     * @Route("/chose-number-of-players", name="chose_number_of_players")
     */
    public function index()
    {
        return $this->render('create_new_season/index.html.twig', [
            'controller_name' => 'CreateNewSeasonController',
        ]);
    }

    /**
     * @Route("/players-names-forms", name="players_names_forms")
     */
    public function createUsernameFormsAction(Request $request)
    {
        $amountOfPlayers = $request->request->get('playersNumber');

        $result = $this->newSeasonService->verifyNumberOfPlayers($amountOfPlayers);

        if ($result === true) {
            return $this->render('create_new_season/playersNamesForm.html.twig', [
                'numberOfPlayers' => $amountOfPlayers
            ]);
        }

        return $this->redirectToRoute('chose_number_of_players');
    }

    /**
     * @Route("/create-season-and-players", name="create_season_and_players")
     */
    public function createTeams(Request $request)
    {
        $result = $request->request->all();

        $insert = $this->newSeasonService->createNewSeasonAndAddPlayers($result);

        if (is_integer($insert)) {
            return $this->redirectToRoute('show_player_list', [
                'seasonNumber' => $insert
            ]);
        }

        return $this->redirectToRoute('chose_number_of_players');
    }

    /**
     * @Route("/show-player-list/{seasonNumber}", name="show_player_list")
     */
    public function displayPlayers($seasonNumber)
    {
        $players = $this->newSeasonService->showPlayersInParticularSeason($seasonNumber);

        return $this->render('create_new_season/playersList.html.twig', [
            'players' => $players,
            'seasonId' =>$seasonNumber
            ]);
    }
}
