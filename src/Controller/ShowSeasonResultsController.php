<?php

namespace App\Controller;

use App\SeasonResult\SeasonResultService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/show-seasons-result")
 */
class ShowSeasonResultsController extends AbstractController
{
    private $seasonResultService;

    public function __construct(SeasonResultService $seasonResultService)
    {
        $this->seasonResultService = $seasonResultService;
    }

    /**
     * @Route("/results-list", name="results_list")
     */
    public function index()
    {
        $seasons = $this->seasonResultService->showSeasonList();

        return $this->render('show_season_results/index.html.twig', [
            'seasons' => $seasons
        ]);
    }

    /**
     * @Route("/best-player-in-season/{seasonNumber}", name="best_player_in_season")
     */
    public function eachSeasonResultAction($seasonNumber)
    {
        $scores = $this->seasonResultService->showBestPlayerList($seasonNumber);

        return $this->render('show_season_results/bestPlayerInSeason.html.twig', [
            'seasonNumber' => $seasonNumber,
            'scores' => $scores
        ]);
    }

    /**
     * @Route("/all-games-in-season/{seasonNumber}", name="all_games_in_season")
     */
    public function allGamesInSeasonAction($seasonNumber)
    {
        $scores = $this->seasonResultService->showAllGamesInSeason($seasonNumber);

        return $this->render('show_season_results/allGamesInSeason.html.twig', [
            'seasonNumber' => $seasonNumber,
            'scores' => $scores
        ]);
    }
}

