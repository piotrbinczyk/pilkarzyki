<?php

namespace App\Controller;

use App\GenerateMatches\GenerateMatchesService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/create-new-matches")
 */
class CreateMatchesController extends AbstractController
{
    private $matchesService;

    public function __construct(GenerateMatchesService $matchesService)
    {
        $this->matchesService = $matchesService;
    }

    /**
     * @Route("/create-teams/{seasonId}", name="create_teams")
     */
    public function index($seasonId)
    {
        $matchesExist = $this->matchesService->checkIfMatchesCreated($seasonId);

        if ($matchesExist == 0) {
            $this->matchesService->createTeams($seasonId);
        }
            return $this->redirectToRoute('show_matches', [
                'seasonId' => $seasonId
            ]);
    }

    /**
     * @Route("/showMatches/{seasonId}", name="show_matches")
     */
    public function showMatchesAction($seasonId)
    {
        $competition = $this->matchesService->showMatchesBetweenTeams($seasonId);

        return $this->render('create_matches/index.html.twig', [
            'competition' => $competition,
            'seasonId' => $seasonId
        ]);
    }

    /**
     * @Route("/playMatches/{seasonId}", name="play_matches")
     */
    public function playMatches($seasonId)
    {
        $seasonEnd = $this->matchesService->checkIfSeasonEnded($seasonId);

        if ($seasonEnd[0]['endTime'] !== null) {
            return $this->redirectToRoute('all_games_in_season', [
                'seasonNumber' => $seasonId
            ]);
        }

        $gameResults = $this->matchesService->generateGames($seasonId);

        return $this->render('create_matches/playMatches.html.twig', [
            'gameResults' => $gameResults
        ]);
    }
}
