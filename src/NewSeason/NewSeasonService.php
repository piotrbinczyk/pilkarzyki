<?php

namespace App\NewSeason;

class NewSeasonService
{
    private $player1 = 0;
    private $player2 = 0;
    private $player3 = 0;
    private $player4 = 0;

    private $eachPlayer = [];

    private $commandRepository;
    private $queryRepository;

    public function __construct(
        NewSeasonCommandRepository $commandRepository,
        NewSeasonQueryRepository $queryRepository
    ) {
        $this->commandRepository = $commandRepository;
        $this->queryRepository = $queryRepository;
    }

    public function verifyNumberOfPlayers(int $players): bool
    {

        if ($players > 3 && $players < 9) {
            return true;
        }

        return false;
    }

    public function setTeams(array $players)
    {
        foreach ($players as $player) {
            array_push($this->eachPlayer, $player);
        }

        for ($i = 1; $i <= count($this->eachPlayer)-1; $i++) {
            $this->player1 = $this->eachPlayer[0];
            $this->player2 = $this->eachPlayer[$i];

            for ($j = 1; $j <= count($this->eachPlayer)-1; $j++) {
                if ($this->eachPlayer[$j] != $this->player2) {
                    $this->player3 = $this->eachPlayer[$j];

                    for ($k = 1; $k <= count($this->eachPlayer)-1; $k++) {
                        if ($this->eachPlayer[$k] != $this->player2 &&
                            $this->eachPlayer[$k] != $this->player1 &&
                            $this->eachPlayer[$k] != $this->player3
                        ) {
                            $this->player4 = $this->eachPlayer[$k];
                            if ($k == count($this->eachPlayer)-1) {
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    public function createNewSeasonAndAddPlayers(array $players): ?int
    {
        return  $this->commandRepository->createNewSeason($players);
    }

    public function showPlayersInParticularSeason(int $seasonId)
    {
        return $this->queryRepository->findPlayersNamesBySeasonId($seasonId);
    }
}