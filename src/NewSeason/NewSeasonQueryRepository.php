<?php

namespace App\NewSeason;

use Doctrine\ORM\EntityManagerInterface;

class NewSeasonQueryRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findPlayersNamesBySeasonId(int $seasonId)
    {
        $query = $this->entityManager->createQuery(
            'SELECT p.name
                  FROM App\Entity\Player p 
                  INNER JOIN App\Entity\Season s WITH s.seasonId = p.fkSeasonId 
                  WHERE p.fkSeasonId = :seasonId'
        )->setParameter('seasonId', $seasonId);

        return $query->getResult();
    }
}