<?php

namespace App\NewSeason;

use App\Entity\Player;
use App\Entity\Season;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class NewSeasonCommandRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createNewSeason(array $players)
    {
        $this->entityManager->beginTransaction();
        try {
            $season = new Season();
            $season->setStartTime(new \DateTime("now"));
            $this->entityManager->persist($season);

            $this->entityManager->flush();

            $seasonId = $season->getSeasonId();

            foreach ($players as $name) {
                $player = new Player();
                $player->setName($name);
                $player->setFkSeasonId($seasonId);
                $player->setPoints(0);
                $this->entityManager->persist($player);
            }

            $this->entityManager->flush();

            $this->entityManager->getConnection()->commit();
        } catch (Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            return null;
        }

        return $seasonId;
    }
}