CREATE TABLE IF NOT EXISTS `player` (
  `player_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) CHARACTER SET 'utf8' NOT NULL,
  `points` SMALLINT(6) NULL DEFAULT NULL,
  `fk_season_id` INT UNSIGNED NOT NULL,
  FOREIGN KEY (fk_season_id) REFERENCES season(season_id),
  PRIMARY KEY (`player_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_polish_ci;

CREATE TABLE IF NOT EXISTS `season` (
  `season_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `start_time` DATETIME NOT NULL,
  `end_time` DATETIME NULL,
  PRIMARY KEY (`season_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_polish_ci;

CREATE TABLE IF NOT EXISTS `team` (
  `team_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`team_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_polish_ci;

CREATE TABLE IF NOT EXISTS `player_has_team` (
  `player_has_team_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fk_player_id` INT UNSIGNED NOT NULL,
  `fk_team_id` INT UNSIGNED NOT NULL,
  FOREIGN KEY (fk_player_id) REFERENCES player(player_id),
  FOREIGN KEY (fk_team_id) REFERENCES team(team_id),
  PRIMARY KEY (`player_has_team_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_polish_ci;

CREATE TABLE IF NOT EXISTS `matches` (
  `match_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`match_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_polish_ci;

CREATE TABLE IF NOT EXISTS `team_has_match` (
  `team_has_match_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fk_team_id` INT UNSIGNED NOT NULL,
  `fk_match_id` INT UNSIGNED NOT NULL,
  `points` SMALLINT UNSIGNED NOT NULL,
  `result` TINYINT UNSIGNED,
  FOREIGN KEY (fk_team_id) REFERENCES team(team_id),
  FOREIGN KEY (fk_match_id) REFERENCES matches(match_id),
  PRIMARY KEY (`team_has_match_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8
  COLLATE = utf8_polish_ci;

