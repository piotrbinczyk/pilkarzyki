<?php

namespace App\SeasonResult;

use Doctrine\ORM\EntityManagerInterface;

class SeasonResultQueryRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findSeasonByEndTime()
    {
        $query = $this->entityManager->createQuery(
            'SELECT s.seasonId, s.startTime, s.endTime
                  FROM App\Entity\Season s 
                  WHERE s.endTime IS NOT NULL
                  ORDER BY s.seasonId DESC'
        );

        return $query->getResult();
    }

    public function findPlayersByScoreAndSeason(int $seasonId)
    {
        $query = $this->entityManager->createQuery(
            'SELECT p.name, p.points
                  FROM App\Entity\Player p
                  WHERE p.fkSeasonId = :seasonId 
                  ORDER BY p.points DESC'
        )->setParameter('seasonId', $seasonId);

        return $query->getResult();
    }

    public function findGamesResultsBySeason(int $seasonId)
    {
        $query = $this->entityManager->createQuery(
          'SELECT p.name AS playerOne,thm.fkTeamId, thm.fkMatchId, thm.points, thm.result
          FROM App\Entity\Player p
          INNER JOIN App\Entity\Season s WITH s.seasonId = p.fkSeasonId
          INNER JOIN App\Entity\PlayerHasTeam pht WITH pht.fkPlayerId = p.playerId
          INNER JOIN App\Entity\TeamHasMatch thm WITH thm.fkTeamId = pht.fkTeamId
          WHERE s.seasonId = :seasonId 
          ORDER BY thm.fkMatchId, thm.fkTeamId ASC'
        )->setParameter('seasonId', $seasonId);

        return $query->getResult();
    }
}
