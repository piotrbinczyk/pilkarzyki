<?php

namespace App\SeasonResult;

class SeasonResultService
{
    private $gameScores = [];
    private $eachScore = [];

    private $seasonResultQuery;

    public function __construct(SeasonResultQueryRepository $seasonResultQuery)
    {
        $this->seasonResultQuery = $seasonResultQuery;
    }

    public function showSeasonList()
    {
        return $this->seasonResultQuery->findSeasonByEndTime();
    }

    public function showBestPlayerList(int $seasonId)
    {
        return $this->seasonResultQuery->findPlayersByScoreAndSeason($seasonId);
    }


    public function showAllGamesInSeason(int $seasonId): array
    {
        $result =  $this->seasonResultQuery->findGamesResultsBySeason($seasonId);

        $loop = count($result);

        for ($i = 0; $i<= $loop-1; ++$i) {
            if ($i%4 === 0 && $i!== 0) {
                array_push($this->gameScores, $this->eachScore);
                $this->eachScore = [];
            }

            array_push($this->eachScore, $result[$i]);

            if ($i === ($loop-1)) {
                array_push($this->gameScores, $this->eachScore);
            }

        }
        $this->mergeTeamsInScores();

        return $this->gameScores;
    }

    private function mergeTeamsInScores()
    {
        $loop = count($this->gameScores);

        for ($i=0; $i<=$loop-1; $i++) {
            $this->gameScores[$i][0]['playerTwo'] = $this->gameScores[$i][1]['playerOne'];
            $this->gameScores[$i][2]['playerTwo'] = $this->gameScores[$i][3]['playerOne'];
            $this->gameScores[$i][1] = $this->gameScores[$i][2];
            unset($this->gameScores[$i][2]);
            unset($this->gameScores[$i][3]);
        }
    }
}
