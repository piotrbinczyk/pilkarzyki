<?php

namespace App\GenerateMatches;

class GenerateMatchesService
{
    private $queryRepository;
    private $commandRepository;

    private $player1 = 0;

    private $teams = [];
    private $eachPlayer = [];
    private $competition = [];
    private $playerPointsToUpdate = [];

    public function __construct(
        GenerateMatchesQueryRepository $queryRepository,
        GenerateMatchesCommandRepository $commandRepository
    ) {
        $this->queryRepository = $queryRepository;
        $this->commandRepository = $commandRepository;
    }

    public function checkIfMatchesCreated(int $seasonId): int
    {
        $seasons =  $this->queryRepository->countPlayersTeamsBySeasonId($seasonId);

        return $seasons[0][1];
    }

    public function createTeams(int $seasonId): bool
    {
        $result =  $this->queryRepository->findPlayersIdBySeasonId($seasonId);

        return $this->setTeams($result);
    }

    public function setTeams(array $result): bool
    {
        foreach ($result as $player) {
            array_push($this->eachPlayer, $player['playerId']);
        }

        for ($i = 0; $i <= count($this->eachPlayer)-1; $i++) {
            $this->player1 = $this->eachPlayer[$i];

            for ($j = $i+1; $j <= count($this->eachPlayer)-1; $j++) {
                array_push($this->teams, [$this->player1, $this->eachPlayer[$j]]);
            }
        }

        return $this->commandRepository->createTeams($this->teams);
    }

    public function showMatchesBetweenTeams(int $seasonId): array
    {
        $result =  $this->queryRepository->findTeamsBySeasonId($seasonId);

        $this->mergePlayersByTeams($result);

        $this->createCompetition();

        return $this->competition;
    }

    private function mergePlayersByTeams(array $players)
    {
        for ($i=0; $i<count($players)-1; $i++) {
            if ($players[$i]['teamId'] === $players[$i+1]['teamId']) {
                array_push($this->teams, [
                    'teamId' => $players[$i]['teamId'],
                    'player1' => $players[$i]['name'],
                    'player2' => $players[$i+1]['name']
                ]);
            }
        }
    }

    private function createCompetition()
    {
        for ($i=0; $i<count($this->teams); $i++) {
            for ($k=$i; $k<count($this->teams); $k++) {
                $result1 = in_array($this->teams[$i]['player1'], $this->teams[$k]);
                $result2 = in_array($this->teams[$i]['player2'], $this->teams[$k]);
                if ($result1 != true && $result2 != true) {
                    array_push($this->competition, [ $this->teams[$i], $this->teams[$k]]);
                }
            }
        }
    }

    private function randomizePoints(): array
    {
        $score1 = 0;
        $score2 = 0;

        while ($score1 === $score2) {
            $score1 = mt_rand(0, 3);
            $score2 = mt_rand(0, 3);
            if ($score1 !== 3 && $score2 !== 3) {
                $res = mt_rand(0, 1);

                if ($res == 1) {
                    $score1 = 3;
                } else {
                    $score2 = 3;
                }
            }
        }

        return [
          'score1' => $score1,
          'score2' => $score2
        ];
    }

    public function generateGames(int $seasonId): array
    {
        $result =  $this->queryRepository->findTeamsBySeasonId($seasonId);

        $this->mergePlayersByTeams($result);

        $this->createCompetition();

        $loop = count($this->competition);


        for ($i = 0; $i < $loop; $i++) {
            $scores = $this->randomizePoints();
            $this->competition[$i][0]['points'] = $scores['score1'];
            $this->competition[$i][1]['points'] = $scores['score2'];

            if ($scores['score1'] > $scores['score2']) {
                $this->competition[$i][0]['result'] = 1;
                $this->competition[$i][1]['result'] = 0;
                array_push($this->playerPointsToUpdate, $this->competition[$i][0]['teamId']);
            } else {
                $this->competition[$i][0]['result'] = 0;
                $this->competition[$i][1]['result'] = 1;
                array_push($this->playerPointsToUpdate, $this->competition[$i][1]['teamId']);
            }
        }

        foreach ($this->playerPointsToUpdate as $update) {
            $this->commandRepository->updatePlayersPoints($update);
        }

        $this->commandRepository->createMatches($this->competition);

        $this->commandRepository->updateSeasonEndDate(new \DateTime('now'), $seasonId);

        return $this->competition;
    }

    public function checkIfSeasonEnded(int $seasonId)
    {
        return $this->queryRepository->findSeasonEndTimeByIdAndEndTime($seasonId);
    }
}
