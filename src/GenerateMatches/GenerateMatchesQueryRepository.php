<?php

namespace App\GenerateMatches;

use Doctrine\ORM\EntityManagerInterface;

class GenerateMatchesQueryRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findPlayersIdBySeasonId(int $seasonId)
    {
        $query = $this->entityManager->createQuery(
            'SELECT p.playerId
                  FROM App\Entity\Player p 
                  INNER JOIN App\Entity\Season s WITH s.seasonId = p.fkSeasonId 
                  WHERE p.fkSeasonId = :seasonId'
        )->setParameter('seasonId', $seasonId);

        return $query->getResult();
    }

    public function findTeamsBySeasonId(int $seasonId)
    {
        $query = $this->entityManager->createQuery(
            'SELECT p.name, t.teamId
                  FROM App\Entity\Player p 
                  INNER JOIN App\Entity\Season s WITH s.seasonId = p.fkSeasonId
                  INNER JOIN App\Entity\PlayerHasTeam pht WITH pht.fkPlayerId = p.playerId
                  INNER JOIN App\Entity\Team t WITH pht.fkTeamId = t.teamId
                  WHERE s.seasonId = :seasonId 
                  ORDER BY t.teamId'
        )->setParameter('seasonId', $seasonId);

        return $query->getResult();
    }

    public function countPlayersTeamsBySeasonId(int $seasonId)
    {
        $query = $this->entityManager->createQuery(
            'SELECT COUNT(s)
                  FROM App\Entity\Player p
                  INNER JOIN App\Entity\Season s WITH s.seasonId = p.fkSeasonId
                  INNER JOIN App\Entity\PlayerHasTeam pht WITH pht.fkPlayerId = p.playerId
                  WHERE s.seasonId = :seasonId'
        )->setParameter('seasonId', $seasonId);

        return $query->getResult();
    }

    public function findSeasonEndTimeByIdAndEndTime(int $seasonId)
    {
        $query = $this->entityManager->createQuery(
            'SELECT s.endTime
                FROM App\Entity\Season s
                WHERE s.seasonId = :seasonId'
        )->setParameter('seasonId', $seasonId);

        return $query->getResult();
    }
}