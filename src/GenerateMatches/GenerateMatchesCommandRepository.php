<?php

namespace App\GenerateMatches;

use App\Entity\Matches;
use App\Entity\PlayerHasTeam;
use App\Entity\Team;
use App\Entity\TeamHasMatch;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GenerateMatchesCommandRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createTeams(array $teams)
    {
        $this->entityManager->beginTransaction();

        try {
            foreach ($teams as $team) {
                $player1 = $team[0];
                $player2 = $team[1];

                $team = new Team();
                $this->entityManager->persist($team);

                $this->entityManager->flush();

                $teamId = $team->getTeamId();

                $playerHasTeam = new PlayerHasTeam();
                $playerHasTeam->setFkTeamId($teamId);
                $playerHasTeam->setFkPlayerId($player1);
                $this->entityManager->persist($playerHasTeam);

                $playerHasTeam = new PlayerHasTeam();
                $playerHasTeam->setFkTeamId($teamId);
                $playerHasTeam->setFkPlayerId($player2);
                $this->entityManager->persist($playerHasTeam);

                $this->entityManager->flush();
            }

            $this->entityManager->getConnection()->commit();

        } catch (Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            return false;
        }

        return true;
    }

    public function createMatches(array $games)
    {
        $this->entityManager->beginTransaction();

        try {
            foreach ($games as $game) {
                $match = new Matches();
                $this->entityManager->persist($match);
                $this->entityManager->flush();

                $matchId = $match->getMatchId();

                $teamHasMatch = new TeamHasMatch();
                $teamHasMatch->setFkMatchId($matchId);
                $teamHasMatch->setFkTeamId($game[0]['teamId']);
                $teamHasMatch->setPoints($game[0]['points']);
                $teamHasMatch->setResult($game[0]['result']);
                $this->entityManager->persist($teamHasMatch);

                $teamHasMatch = new TeamHasMatch();
                $teamHasMatch->setFkMatchId($matchId);
                $teamHasMatch->setFkTeamId($game[1]['teamId']);
                $teamHasMatch->setPoints($game[1]['points']);
                $teamHasMatch->setResult($game[1]['result']);
                $this->entityManager->persist($teamHasMatch);

                $this->entityManager->flush();
            }

            $this->entityManager->getConnection()->commit();

        } catch (Exception $e) {
            $this->entityManager->getConnection()->rollBack();
            return false;
        }

        return true;
    }

    public function updatePlayersPoints(int $teamId)
    {
        $sql = "UPDATE player
            INNER JOIN player_has_team
            ON player_has_team.fk_player_id = player.player_id
            SET player.points = player.points+1
            WHERE player_has_team.fk_team_id = :teamId";

        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $stmt->execute([':teamId' => $teamId]);
        return $stmt->rowCount();
    }

    public function updateSeasonEndDate(\DateTime $date, int $seasonId)
    {
        $query = $this->entityManager->createQuery(
            'UPDATE App\Entity\Season s 
                  SET s.endTime = :endTime
                  WHERE s.seasonId = :seasonId'
        )->setParameters(array(
            'endTime' => $date,
            'seasonId' => $seasonId
        ));

        return $query->execute();
    }
}
